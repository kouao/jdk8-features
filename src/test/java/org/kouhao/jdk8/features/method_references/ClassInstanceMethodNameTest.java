package org.kouhao.jdk8.features.method_references;

import lombok.AllArgsConstructor;
import lombok.Data;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.function.Function;

/**
 * 类::实例方法名
 *
 * @author kouhao
 */
public class ClassInstanceMethodNameTest {
    static User2 user;

    @BeforeAll
    public static void before() {
        user = new User2(1, "test", "123456");
    }
    /**
     * 正常使用匿名对象
     */
    @Test
    public void test_class_instance_method_name_normal() {
        Function<User2, String> function = new Function<>() {
            @Override
            public String apply(User2 user) {
                return user.getName();
            }
        };
        System.out.println(function.apply(user));
    }

    /**
     * 使用lambda
     */
    @Test
    public void test_class_static_method_name_lambda() {
        Function<User2, String> function = user -> user.getName();
        System.out.println(function.apply(user));
    }

    /**
     * 使用方法引用
     */
    @Test
    public void test_class_static_method_name_method_reference() {
        Function<User2, String> function = User2::getName;
        System.out.println(function.apply(user));
    }
}
/**
 * User
 */
@Data
@AllArgsConstructor
class User2 {
    int id;

    String name;

    String adr;
}
