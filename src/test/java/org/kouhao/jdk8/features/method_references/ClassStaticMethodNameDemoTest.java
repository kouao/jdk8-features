package org.kouhao.jdk8.features.method_references;

import org.junit.jupiter.api.Test;

import java.util.function.Function;

/**
 * 类::静态方法名
 *
 * @author kouhao
 */
public class ClassStaticMethodNameDemoTest {
    /**
     * 正常使用匿名对象
     */
    @Test
    public void test_class_static_method_name_normal() {
        Function<Double,Long> function = new Function<Double,Long>() {
            @Override
            public Long apply(Double dd) {
                return Math.round(dd);
            }
        };
       System.out.println(function.apply(20.32));
    }

    /**
     * 使用lambda
     */
    @Test
    public void test_class_static_method_name_lambda() {
        Function<Double,Long> function = dd -> Math.round(dd);
        System.out.println(function.apply(20.32));
    }

    /**
     * 使用方法引用
     */
    @Test
    public void test_class_static_method_name_method_reference() {
        Function<Double,Long> function = Math::round;
        System.out.println(function.apply(20.32));
    }
}
