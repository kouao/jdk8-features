package org.kouhao.jdk8.features.method_references;

import lombok.AllArgsConstructor;
import lombok.Data;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.function.Supplier;

/**
 * 对象::实例方法名 使用例子
 */
public class ObjectInstanceMethodNameDemoTest {
    static User user;

    @BeforeAll
    public static void before() {
        user = new User(1, "test", "123456");
    }

    /**
     * 对象::实例方法名 正常使用匿名对象
     */
    @Test
    public void test_object_instance_method_name_normal() {
        Supplier<String> supplier = new Supplier<String>() {
            @Override
            public String get() {
                return user.getName();
            }
        };
        System.out.println(supplier.get());
    }

    /**
     * 对象::实例方法名 使用lambda
     */
    @Test
    public void test_object_instance_method_name_lambda() {
        Supplier<String> supplier = () -> user.getName();
        System.out.println(supplier.get());
    }

    /**
     * 对象::实例方法名 使用方法引用
     */
    @Test
    public void test_object_instance_method_name_methodReference() {
        Supplier<String> supplier = user::getName;
        System.out.println(supplier.get());
    }
}

/**
 * User
 */
@Data
@AllArgsConstructor
class User {
    int id;

    String name;

    String adr;
}