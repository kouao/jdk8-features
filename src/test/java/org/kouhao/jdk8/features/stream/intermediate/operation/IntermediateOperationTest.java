package org.kouhao.jdk8.features.stream.intermediate.operation;

import org.junit.jupiter.api.Test;

import java.util.Comparator;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * 中间操作测试
 *
 * @author kouhao
 */
public class IntermediateOperationTest {
    /**
     * 切片操作测试
     */
    @Test
    public void sectioning_test() {
        Stream<String> stream = Stream.of("1", "2", "3");
        Integer data = stream.filter("1"::equals)
            .flatMapToInt(s -> IntStream.of(Integer.parseInt(s)))
            .findFirst()
            .orElse(0);
        System.out.println(data);
    }

    /**
     * Stream map操作
     */
    @Test
    public void map_test() {
        Stream<String> stream = Stream.of("1", "2", "3");
        Stream<String> map = stream.map(s -> {
            if ("2".equals(s)) {
                return s;
            } else {
                return "0";
            }
        }).distinct().sorted(Comparator.reverseOrder());
        map.forEach(System.out::println);
    }
}
