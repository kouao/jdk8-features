package org.kouhao.jdk8.features.stream.create;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.Stream;

/**
 * 创建流
 *
 * @author kouhao
 */
public class CreateStreamTest {
    /**
     * 通过集合创建stream
     */
    @Test
    public void collection_stream_test() {
        ArrayList<String> list = new ArrayList<>() {{
            add("1");
            add("2");
            add("3");
        }};
        System.out.println(list.stream().findFirst().orElse(null));
    }

    /**
     * 通过数组工具创建stream
     */
    @Test
    public void array_stream_test() {
        Stream<String> stream = Arrays.stream(new String[] {"1", "2", "3"});
        System.out.println(stream.findFirst().orElse(null));
    }

    /**
     * 通过Stream.of()
     */
    @Test
    public void stream_of_test() {
        Stream<String> stream = Stream.of("1", "2", "3");
        System.out.println(stream.findFirst().orElse(null));
    }

    /**
     * 通过Stream.iterate()
     */
    @Test
    public void stream_iterate_test() {
        Stream<String> stream = Stream.iterate("1", s -> s.length() != 10, s -> {
            s = s + "1";
            System.out.println(s);
            return s;
        });
        System.out.println(stream.count());
    }

    /**
     * 通过Stream.generate()
     */
    @Test
    public void stream_generate_test() {
        Stream<String> stream = Stream.generate(() -> "null").limit(10);
        System.out.println(stream.count());
    }
}
