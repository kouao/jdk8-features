package org.kouhao.jdk8.features.stream.terminating.operations;

import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Stream 终结操作
 *
 * @author kouhao
 */
public class TerminatingOperationsTest {
    /**
     * reduce 操作
     */
    @Test
    public void reduce_test() {
        Stream<String> stream = Stream.of("1", "2", "3");
        Integer data = stream.parallel().map(Integer::parseInt).reduce(Integer::sum).get();
        System.out.println(data);
    }

    /**
     * collect 操作
     */
    @Test
    public void collect_test() {
        Stream<Integer> stream = Stream.of(1, 2, 3, 10, 20);
        List<Integer> list = stream.filter(integer -> integer >= 3).collect(Collectors.toList());
        System.out.println(list);
    }
}
