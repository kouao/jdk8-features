package org.kouhao.jdk8.features.constructor.reference;

import org.junit.jupiter.api.Test;

/**
 * 构造器引用
 *
 * @author kouhao
 */
public class ConstructorReferenceTest {
    /**
     * 匿名类方式实现
     */
    @Test
    public void test_normal() {
        PersonFactory<Person> personFactory = new PersonFactory<>() {
            @Override
            public Person create(String firstName, String lastName) {
                return new Person(firstName, lastName);
            }
        };
        System.out.println(personFactory.create("kou", "hao").firstName);
    }

    /**
     * lambda表达式
     */
    @Test
    public void test_lambda() {
        PersonFactory<Person> personFactory = (firstName, lastName) -> new Person(firstName, lastName);
        System.out.println(personFactory.create("kou", "hao").firstName);
    }

    /**
     * 构造方法引用
     */
    @Test
    public void test_constructor_reference() {
        PersonFactory<Person> personFactory = Person::new;
        System.out.println(personFactory.create("kou", "hao").firstName);
    }
}

interface PersonFactory<R extends Person> {
    R create(String firstName, String lastName);
}

class Person {
    String firstName;

    String lastName;

    Person() {
    }

    Person(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }
}
