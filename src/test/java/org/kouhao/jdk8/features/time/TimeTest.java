package org.kouhao.jdk8.features.time;

import org.junit.jupiter.api.Test;

import java.time.Clock;
import java.time.DayOfWeek;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.time.temporal.ChronoField;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.Date;
import java.util.Locale;
import java.util.Set;

/**
 * JDK8 日期新API
 *
 * @author kouhao
 */
public class TimeTest {
    @Test
    public void testClock() {
        // 获取一个时钟，该时钟使用最佳可用系统时钟返回当前瞬间，并使用默认时区转换为日期和时间。
        // 此时钟基于最佳可用系统时钟。这可以使用System.currentTimeMillis()，或者更高分辨率的时钟(如果有的话)。
        // 使用此方法将对默认时区的依赖项硬编码到应用程序中。建议避免这种情况，并尽可能使用特定的时区。
        // 当您需要当前时刻而不需要日期或时间时，应该使用UTC时钟。
        Clock clock = Clock.systemDefaultZone();
        long millis = clock.millis();
        System.out.println(new Date(millis));
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        // 时间戳
        Instant instant = clock.instant();
        Date legacyDate = Date.from(instant);
        System.out.println(legacyDate);
    }

    /**
     * TimeZone
     */
    @Test
    public void testTimeZone() {
        Set<String> set = ZoneId.getAvailableZoneIds();
        System.out.println(Arrays.toString(set.toArray()));

        ZoneId zone1 = ZoneId.of("Europe/Berlin");
        ZoneId zone2 = ZoneId.of("Brazil/East");
        System.out.println(zone1.getRules());// ZoneRules[currentStandardOffset=+01:00]
        System.out.println(zone2.getRules());// ZoneRules[currentStandardOffset=-03:00]
    }

    /**
     * LocalTime
     */
    @Test
    public void testLocalTime() {
        // 德国柏林
        ZoneId zone1 = ZoneId.of("Europe/Berlin");
        LocalTime now1 = LocalTime.now(zone1);
        ZoneId zone2 = ZoneId.of("Brazil/East");
        LocalTime now2 = LocalTime.now(zone2);

        long hoursBetween = ChronoUnit.HOURS.between(now1, now2);
        long minutesBetween = ChronoUnit.MINUTES.between(now1, now2);

        System.out.println(hoursBetween); // -3
        System.out.println(minutesBetween); // -239

        LocalTime late = LocalTime.of(23, 59, 59);
        System.out.println(late); // 23:59:59

        DateTimeFormatter germanFormatter = DateTimeFormatter.ofLocalizedTime(FormatStyle.SHORT)
            .withLocale(Locale.GERMAN);
        LocalTime time = LocalTime.parse("13:37", germanFormatter);
        System.out.println(time); // 13:37
    }

    /**
     * LocalDate
     */
    @Test
    public void testLocalDate() {
        LocalDate today = LocalDate.now();
        // 加
        LocalDate tomorrow = today.plus(1, ChronoUnit.DAYS);
        System.out.println(tomorrow);// 2023-11-06
        // 减
        LocalDate yesterday = tomorrow.minusDays(2);
        System.out.println(yesterday);// 2023-11-04

        LocalDate independenceDay = LocalDate.of(2023, Month.NOVEMBER, 5);
        DayOfWeek dayOfWeek = independenceDay.getDayOfWeek();
        System.out.println(dayOfWeek); // SUNDAY

        DateTimeFormatter germanFormatter = DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM)
            .withLocale(Locale.GERMAN);
        LocalDate xmas = LocalDate.parse("24.12.2014", germanFormatter);
        System.out.println(xmas); // 2014-12-24
    }

    /**
     * LocalDateTime测试
     */
    @Test
    public void testLocalDateTime() {
        LocalDateTime sylvester = LocalDateTime.of(2014, Month.DECEMBER, 31, 23, 59, 59);

        DayOfWeek dayOfWeek = sylvester.getDayOfWeek();
        System.out.println(dayOfWeek); // WEDNESDAY

        Month month = sylvester.getMonth();
        System.out.println(month); // DECEMBER
        // 一天的分钟
        long minuteOfDay = sylvester.getLong(ChronoField.MINUTE_OF_DAY);
        System.out.println(minuteOfDay); // 1439

        // 设置时区信息
        Instant instant = sylvester.atZone(ZoneId.systemDefault()).toInstant();
        Date legacyDate = Date.from(instant);
        System.out.println(legacyDate); // Wed Dec 31 23:59:59 CST 2014

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM dd, yyyy - HH:mm");
        LocalDateTime parsed = LocalDateTime.parse("11 03, 2014 - 07:13", formatter);
        System.out.println(parsed);// 2014-11-03T07:13
        String string = formatter.format(parsed);
        System.out.println(string); // 11 03, 2014 - 07:13
    }
}
