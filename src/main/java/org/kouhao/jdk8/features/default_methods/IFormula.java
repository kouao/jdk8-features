package org.kouhao.jdk8.features.default_methods;

/**
 * @author kouhao
 */
public interface IFormula {
    double calculate(int a);

    default double sqrt(int a) {
        return Math.sqrt(a);
    }
}
