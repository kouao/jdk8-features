package org.kouhao.jdk8.features.default_methods;

/**
 * @author kouhao
 */
public class Client {
    public static void main(String[] args) {
        IFormula formula = new IFormula() {
            @Override
            public double calculate(int a) {
                return a * a;
            }
        };
        System.out.println(formula.calculate(2));
    }
}
