package org.kouhao.jdk8.features.functional_interfaces;

/**
 * 自定义函数式接口
 *
 * @author kouhao
 */
@FunctionalInterface
public interface IConverter<T, R> {
    R convert(T from);
}
