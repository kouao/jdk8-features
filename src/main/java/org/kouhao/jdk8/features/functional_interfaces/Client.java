package org.kouhao.jdk8.features.functional_interfaces;

/**
 * @author kouhao
 */
public class Client {
    public static void main(String[] args) {
       IConverter<Integer, String> converter = from -> String.valueOf(from);
       System.out.println(converter.convert(2));
    }
}
